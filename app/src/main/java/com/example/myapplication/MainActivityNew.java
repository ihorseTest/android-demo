package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.example.MyUtil;

public class MainActivityNew extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_new);

        MyUtil myUtil = new MyUtil();

        Log.e("Hello:", myUtil.addition("This is ", "sample..." ));
    }
}